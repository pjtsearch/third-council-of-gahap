:white_check_mark:

# **Definitions**

**November 25, 2019**

- :white_check_mark: : This Document or Article is Protected by a Majority vote of the TCG.

- :heavy_check_mark: : This Document or Article is Protected by a Unanimous vote of the TCG.

- :no_entry_sign: : This Document or Article can never be modified.

- Ability: Action allowed for by the TCGC.

- Article: A section of a Document denoted by a number.

- Building Phase: A state of a State Property allowing it to be Mutated.

- Council: Alias for TCG.

- Contract: A legal, signed promise between two entities.

- Document: A Markdown file in this repository.

- Equal Amount of Property for the Levying of Taxes: Currently 1 Standard Price.

- Legislation: Either referring to these set of documents or an alias for Article.

- Majority: 

  ```javascript
  (numberOfMembersInTCG) => Math.floor(numberOfMembersInTCG / 2) + 1
  ```

- Meting of the TCG: A meeting in the designated Council room.

- Mutation: The changing in physical any way of Property.

- Natural Right: A God-given, inherent right of man.

- Property: Any object owned by a TCGC.

- Protected: This requires at least the said amount of votes to change.

- :heavy_check_mark: Standard Price: Currently 1 iron ingot.

- State: Alias for TCG.

- Style Questioning: A TCG Meeting to decide whether a Property fits the style of GAHAP.

- Reasonable Defense: Killing a Citizen who is mutating you or your property, or otherwise forcing them out of your property.

- Regulation: An legal violation of a Natural Right or Ability.

- TCG: The Third Council of GAHAP.

- TCGC: The citizens of the TCG.

- Trespassing: The act of a TCGC's position being inside the bounds of said Property.

- Unanimous:

  ```javascript
  (numberOfMembersInTCG) => numberOfMembersInTCG
  ```

  


