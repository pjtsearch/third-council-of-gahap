:white_check_mark:

# **State Property Contract**

**November 25, 2019**

1. Tresspassing

   All TCGC's are allowed to Tresspass upon State Property.

2. Mutation

   Mutation of State Property is only allowed when said Property is declared by the TCG as being in Building Phase.