:white_check_mark:

# **Regulations of the State**

**November 25, 2019**

The following are the Regulations of the TCG over its Abilities

1. Amount of Voters

   By default, the Majority of the TCG must vote for a decision of the TCG.  However, for Protected Documents or Protected Articles, the amount of voters required by the Protected Document or Protected Articles must consent.  New Documents must have at least the same Protection level as the highest Protection level in the same folder, unless decided for it to be different in a Unanimous vote of the TCG.

2. State Property Contract

   All state property must follow the [State Property Contract](state-property-contract.md).