:heavy_check_mark:

# **Abilities of the State**

**November 25, 2019**

The following are the Abilities of the State

1. The Creation of Legislation

   The TCG has the ability to create Legislation.
   
2. The Selling of Property

   The TCG is able to sell its property for the Standard Price per square block to any TCGC.
   
3. The Levying of Taxes

   The TCG has the ability to take an Equal Amount of Property from each TCGC daily.


