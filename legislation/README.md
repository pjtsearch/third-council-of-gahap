:no_entry_sign:
# **Establishment of the State and of the Legislation**

**November 25, 2019**

1. Enactment of the State and the Legislation

   At the time of the signing of this document by all TCGC's and henceforth, the TCG and the Legislation is established and active.

2. Ownership of this Legislation

   The legislation is hereby owned by all TCGC's.  It can only be changed by the TCG.
   
3. Legislation Organization

   The following is the organization of the legislation
   
   - [Definitions](definitions.md): Definitions of terms used in this Legislation.  These definitions are the only that can be used for interpretation of the Legislation.
   - [Regarding the Citizen](regarding-the-citizen): Legislation regarding the TCGC.
   
     - [Regulations](regarding-the-citizen/regulations): Regulations **over** the rights of the TCGC.
     - [Rights](regarding-the-citizen/rights): Rights of the TCGC.

   - [Regarding the State](regarding-the-state): Legislation regarding the TCG.

     - [Abilities](regarding-the-state/abilities): Abilities of the TCGC.
     - [Regulations](regarding-the-state/regulations): Regulations **over** the abilities of the TCGC.

### **Signatures**

Chairman Peter of GAHAP

Իշխան Գրիգոր ԿԱՀԱՓ-ին

Baron Aram of GAHAP

Baron Arek of GAHAP

Baron Haig of GAHAP