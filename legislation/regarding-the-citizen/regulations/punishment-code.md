:white_check_mark:

# **The Punishment Code**

**November 27, 2019**

The following are the due punishments for violations of the Legislation

1. Destruction of Built Property
   1. Guilty must give all of the items needed to rebuild back to the owner.
   2. Minimum 5 minutes jail time.
   3. Variable extra fine of Property to give to the TCG.
2. Addition to Built Property
   1. Guilty must remove the added items.
   2. Minimum 5 minutes jail time.
   3. Variable extra fine of Property to give to the TCG.
3. Stealing of Property
   1. Guilty must give items back to their owner.
   2. Minimum 2 minutes jail time.
   3. Variable extra fine of Property to give to the TCG.
4. Killing of a Citizen
   1. Minimum 10 minutes jail time.
   2. Variable extra fine of Property to give to the TCG.
5. Mansluaghter
   1. Minimum 20 minutes jail time.
   2. Variable extra fine of Property to give to the TCG and/or the Citizens that were killed.
