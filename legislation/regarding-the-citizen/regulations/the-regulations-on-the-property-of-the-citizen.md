:heavy_check_mark:

# **The Regulations on the Property of the Citizen**

**November 26, 2019**

The following are the Regulations on the Property of the TCGC

1. Levying of Taxes

   The TCGC must give the Equal Amount of Property to the TCG daily.

2. Style Questioning

   The TCG has the ability to force mutations on the Property of a TCGC for it to comply with a Style Questioning.

