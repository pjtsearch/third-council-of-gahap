:heavy_check_mark:

# **The Regulations on the Citizen**

**November 25, 2019**

The following are Regulations on the TCGC over their Natural Rights and their Abilities

1. Prosecution

   The TCG has the ability to enforce [Due Punishment](punishment-code.md) on any TCGC that violates any Regulation of themselves or Right of another or others.

3. Attendance of the Meetings of the TCG

   The TCGC is obligated to attend the Meetings of the TCG.