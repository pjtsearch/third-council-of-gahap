:no_entry_sign:

# **The Protected Natural Rights of the Citizen**

**November 25, 2019**

The following are the protected Natural Rights of the TCGC

1. The Right to Property

   The TCGC has the Natural Right to complete ownership of Property, including themselves.  The owned Property of the TCGC can not be Trespassed on or Mutated in any way unless consent is given in a Contract.

2. The Freedom of Opinion

   The TCGC has the Natural Right of their opinion and the expression of their opinion.

3. The Defense of Property

   The TCGC has the Natural Right to the Reasonable Defense of their Natural Right of Property.

