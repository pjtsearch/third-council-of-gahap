:white_check_mark:

# **The Additional Abilities of the Citizen**

**November 25, 2019**

The following are the additional Abilities of the TCGC

1. The Ability of Suffrage

   The TCGC has one (1) vote in the TCG.

2. The Ability to Request a Style Questioning

   The TCGC has the ability to request a Style Questioning.
   
3. The Ability to Declare a TCG Meeting

   The TCGC has the right to declare a TCG Meeting.